

angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'beauby.jsonApiDataStore'])

.run(function ($ionicPlatform, $http) {
    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && cordova.platformId === 'ios' && window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }

        var e = document.createElement("script");
        e.setAttribute("src", "http://192.168.1.254:8081/target/target-script-min.js#anonymous");
        document.getElementsByTagName("body")[0].appendChild(e);
        
    });
    
})

.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $stateProvider

    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
    })

    .state('app.search', {
        url: '/search',
        views: {
            'menuContent': {
                templateUrl: 'templates/search.html'
            }
        }
    })

    .state('app.scan', {
        url: '/scan',
        views: {
            'menuContent': {
                templateUrl: 'templates/scan.html',
                controller: 'ScanCtrl'
            }
        }
    })
      .state('app.products', {
          url: '/products',
          views: {
              'menuContent': {
                  templateUrl: 'templates/products.html',
                  controller: 'ProductsCtrl'
              }
          }
      })

    .state('app.product', {
        url: '/products/:productId',
        views: {
            'menuContent': {
                templateUrl: 'templates/product.html',
                controller: 'ProductCtrl'
            }
        }
    })

    .state('app.images', {
        url: '/products/:productId/images',
        views: {
            'menuContent': {
                templateUrl: 'templates/images.html',
                controller: 'ProductCtrl'
            }
        }
    })
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/products');

    $httpProvider.interceptors.push(function(){
        return {
            request: function(config) {                
                if(config.url.match(/\/api\/v1\//))
                {
                    config.headers['Content-Type'] = 'application/vnd.api+json'
                    config.headers['Accept'] = 'application/vnd.api+json'
                    if (window.device)
                    {
                        config.headers['X-Device-Id'] = device.uuid;
                        config.headers['X-Device-Platform'] = device.platform;
                    }
                }                    
                
                return config;
            }
        }
    })
 
})

angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

})

.controller('ProductsCtrl', function($scope, FoodStaff) {
    $scope.products = [];
        
    $scope.$watch('query', function (value) {
        if (value && value.length > 0) {
            FoodStaff.filter(value).then(function (items) {
                $scope.products = items;
            })
        } else {
            $scope.products = []
        }
    })
})

.controller('ProductCtrl', function ($scope, $stateParams, $log, $ionicPopup, $ionicScrollDelegate, DG, FoodStaff) {
    FoodStaff.get($stateParams.productId).then(function (product) {        
        $scope.product = product;
        
    }, function() {
        if($stateParams.productId.length > 12)
        {
            $scope.product = FoodStaff.new({ barcode: $stateParams.productId })
            $scope.new_cost = null
            $scope.current_spinner = ''
            $scope.images = {}
        }
    })

    //4601075347524

    $scope.doRefresh = function () {
        if ($scope.product.id)
        {
            $scope.product.sync().finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
            });
        } else {
            $scope.$broadcast('scroll.refreshComplete');
        }                 
    };

    $scope.complete = function () {
        var cost = $scope.product.new_cost;
        $scope.product.create().then(function (product) {
            $scope.product = product;
            $scope.product.setCost(cost)

        })
    }

    $scope.addImage = function (kind) {
        $scope.current_spinner = kind
        setTimeout(function () {
            console.debug("Start Add Image");
            $scope.product.addImage(kind || 'other').then(function (image) {
                $scope.images[kind] = image.image;                
                $scope.current_spinner = ''
            });
        }, 1);
    }

    $scope.updatePrice = function () {
        $scope.data = { current_adr: {} };

        DG.search().then(function (data) {
            $scope.data.adrs = data;
        })

        // An elaborate, custom popup
        var pricePopup = $ionicPopup.show({
            //template: '<input type="number" ng-model="data.cost">',
            templateUrl: 'templates/price_popup.html',
            title: 'Введите цену товара',
            subTitle: 'только целую часть',
            scope: $scope,
            buttons: [
              { text: 'Отмена' },
              {
                  text: '<b>Готово</b>',
                  type: 'button-positive',
                  onTap: function (e) {
                      if (!$scope.data.cost) {
                          e.preventDefault();
                      } else {
                          return $scope.data.cost;
                      }
                  }
              }
            ]
        });

        pricePopup.then(function (cost) {
            if (cost)
            {
                $scope.product.setCost(cost, { 'dg-id': $scope.data.current_adr.id, name: $scope.data.current_adr.name })
            }
        });
    }

    window.current_scope = $scope;
    window.dg = DG;
})

.controller('ScanCtrl', function ($scope, $location, $stateParams, $log) {
    $scope.scan = function () {
        console.debug("Start SCAN")

        function success(result) {            
            $scope.result = result
            if (result.cancelled) {
                return;
            }
            if (result.format == 'EAN_13') {
                $location.path("/app/products/" + result.text);
            } else {
                $scope.scan();
            }
        }

        if (cordova && cordova.plugins.barcodeScanner) {
            cordova.plugins.barcodeScanner.scan(success, function (error) {
                console.debug("Scanning failed: " + error);
            });
        } else {
            success({ cancelled: false, text: '4601075347524', format: 'EAN_13' });
        }
    }
})
angular.module('starter.services', [])

.factory('DG', function ($http, $q, Location) {
    var APIKey = 'ruzqvd2307';
    var apiUrl = 'http://catalog.api.2gis.ru/search';

    function buildUrl(params) {
        var url = apiUrl + "?";
        params['version'] = 1.3
        params['key'] = APIKey
        params['sort'] = 'distance'

        var query = '';

        for(attr in params)
        {
            if(query.length != 0)
            {
                query += '&';
            }

            query += attr + '=' + encodeURIComponent(params[attr])
        }

        return url + query;
    }

    var api = {
        search: function () {

            var defered = $q.defer();
            Location.get().then(function (pos) {
                var url = buildUrl({
                    point: pos.longitude + ',' + pos.latitude,
                    radius: '250',
                    what: 'продукты'
                });

                $http.get(url).then(function (r) {
                    var data = r.data
                    if (data.response_code = '200')
                    {
                        defered.resolve(data.result);
                    } else {
                        defered.reject();
                    }                    
                }, function () {
                    defered.reject();
                });
            }, function () {
                defered.reject();
            })

            return defered.promise;
        }
    }

    return api;
})

angular.module('starter.services')

.factory('FoodStaff', function ($http, JsonApiDataStore, $q, $log, Price, Image) {

    var apiUrl = 'http://prodlook.dev.ajieks.ru/api/v1/food-staffs/';

    var FoodStaff = function (data) {
        if (data) {
            this.setData(data);
        }
    };

    var includes = '?include=promo-image,current-price,images,last-prices'

    FoodStaff.prototype.setData = function (data) {
        angular.extend(this, data);
        if (data.serialize) {
            this.serialize = data.serialize
        }
        if (data.setRelationship) {
            this.setRelationship = data.setRelationship
        }
    };

    FoodStaff.prototype.setCost = function (cost, values) {
        values = typeof values !== 'undefined' ? values : {};
        values['cost'] = cost
        var newCost = new Price(this, values)
        var staff = this;
        return newCost.create().then(function () {
            staff.sync();
        });
    };

    FoodStaff.prototype.minPrice = function () {
        return this['last-prices'][0];
    };

    FoodStaff.prototype.maxPrice = function (cost, values) {
        return this['last-prices'][this['last-prices'].length - 1];
    };

    FoodStaff.prototype.delete = function () {
        $http.delete(apiUrl + this.id).success(function () {
            // ���-������ ������������ �������� ������
        }).error(function (data, status, headers, config) {
            // ���-������ ������ � ������ ������
        });
    };

    FoodStaff.prototype.sync = function () {
        var _ = this;
        return $http.get(apiUrl + this.id + includes).success(function (data) {
            var staff = JsonApiDataStore.store.sync(data);
            _.setData(staff);
        }).error(function (data, status, headers, config) {
            // ���-������ ������ � ������ ������
        });
    };

    FoodStaff.prototype.update = function () {
        return $http.put(apiUrl + this.id, this).success(function () {
            // ���-������ ������������ �������� ������
        }).error(function (data, status, headers, config) {
            // ���-������ ������ � ������ ������
        });
    };

    FoodStaff.prototype.create = function () {
        var data = this.serialize();
        var deferred = $q.defer();
        var _ = this;
        $http.post(apiUrl + includes, JSON.stringify(data)).success(function (r) {
            var staff = JsonApiDataStore.store.sync(r);
            _.setData(staff);
            deferred.resolve(_);
        }).error(function (data, status, headers, config) {
            deferred.reject(data);
        });
        return deferred.promise
    };

    FoodStaff.prototype.addExImage = function (newImage) {
        if (this.images) {
            
            this.images.push(newImage)
        } else {
            this.setRelationship('images', [newImage]);
        }
    }

    FoodStaff.prototype.addImage = function (kind) {
        var newImage = new Image(this, { kind: kind || 'other' });
        var defered = $q.defer();

        $log.debug("Add image with kind: " + newImage.kind);

        var _ = this;

        newImage.captureImage().then(function (imageData) {
            newImage.create(imageData).then(function () {
                console.debug("Image created");
                _.addExImage(newImage);
                defered.resolve(newImage);
            }, function () {
                defered.reject();
            })
        }, function () {
            defered.reject();
        })
        return defered.promise;

    };

    FoodStaff.prototype.imageByKind = function (kind) {;
        for(var i = 0; i < this.images.length; i++){
            if(this.images[i].kind == kind)
            {
                return this.images[i];
            }
        }
        return null;
    };

    var attrs = ['barcode']

    var food_staffs = {
        new: function (values) {
            values = typeof values !== 'undefined' ? values : {};

            var newModel = new JsonApiDataStore.Model('food-staffs')
            attrs.forEach(function (attr) {
                if (values[attr]) {
                    newModel.setAttribute(attr, values[attr]);
                } else {
                    newModel.setAttribute(attr, '');
                }
            })
            return new FoodStaff(newModel);
        },
        filter: function(filter){
            var deferred = $q.defer();
            var scope = this;
            var staffs = [];

            $http.get(apiUrl + "?filter[query]=" + encodeURIComponent(filter)).then(function (response) {
                JsonApiDataStore.store.sync(response.data).forEach(function (data) {
                    staffs.push(new FoodStaff(data));
                });
                deferred.resolve(staffs);
            }, function () {
                deferred.reject();
            })

            return deferred.promise;
        },
        all: function () {
            var deferred = $q.defer();
            var scope = this;
            var staffs = [];

            $http.get(apiUrl).then(function (response) {
                JsonApiDataStore.store.sync(response.data).forEach(function (data) {
                    staffs.push(new FoodStaff(data));
                });
                deferred.resolve(staffs);
            }, function () {
                deferred.reject();
            })

            return deferred.promise;
        },
        get: function (id) {
            var deferred = $q.defer();
            var scope = this;
            var data = {};
            $http.get(apiUrl + id + includes).success(function (data) {
                var staff = JsonApiDataStore.store.sync(data);
                deferred.resolve(new FoodStaff(staff));
            })
            .error(function () {
                deferred.reject();
            });
            return deferred.promise;
        }
    };

    return food_staffs;
})

angular.module('starter.services')

.factory('Image', function ($http, JsonApiDataStore, $q, $log) {
    var apiUrl = 'http://prodlook.dev.ajieks.ru/api/v1/';

    var attrs = ['kind']

    var Image = function (imageable, values) {
        values = typeof values !== 'undefined' ? values : {};

        var newModel = new JsonApiDataStore.Model('images')
        attrs.forEach(function (attr) {
            if (values[attr]) {
                newModel.setAttribute(attr, values[attr]);
            } else {
                newModel.setAttribute(attr, '');
            }
        })

        if (imageable) {
            newModel.setRelationship('imageable', imageable);
        }
        this.setData(newModel);
    };

    Image.prototype.setData = function (data) {
        angular.extend(this, data);
        if (data.serialize) {
            this.serialize = data.serialize
        }
    };

    Image.prototype.apiUrl = function () {
        if (this.imageable && this.imageable.id) {
            return apiUrl + this.imageable._type + '/' + this.imageable.id + '/images/'
        } else {
            return apiUrl + "images/";
        }
    };

    Image.prototype.captureImage = function () {
        var defered = $q.defer();
        console.debug("Capture image start");
        navigator.camera.getPicture(function (imageData) {
            console.debug("Captured image: " + imageData);
            defered.resolve(imageData);
        }, function (message) {
            defered.reject(message);
        }, { quality: 50, destinationType: Camera.DestinationType.FILE_URI });
        return defered.promise;
    };

    Image.prototype.create = function (imageURI) {
        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
        options.mimeType = "image/jpeg";

        options.params = { kind: this.kind };
        options.chunkedMode = false;

        var defered = $q.defer()

        var _ = this;

        var ft = new FileTransfer();
        ft.upload(imageURI, this.apiUrl(), function (r) {
            data = JSON.parse(r.response);
            _.id = data.id;
            _.sync().then(function () {
                defered.resolve()
            }, function () {
                defered.reject()
            });
        }, function (e) {
            console.error(e);
            defered.reject();
        }, options);

        return defered.promise;
    };

    Image.prototype.sync = function () {
        var _ = this;
        return $http.get(this.apiUrl() + this.id).success(function (data) {
            var image = JsonApiDataStore.store.sync(data);
            _.setData(image);
        }).error(function (data, status, headers, config) {
            // Что-нибудь делаем в случае ошибки
        });
    };

    return Image;

})
angular.module('starter.services')

.factory('Location', function ($q) {

    var api = {
        get: function () {
            var defered = $q.defer();
            var onSuccess = function (position) {      
                defered.resolve(position.coords);
            };

            var onError = function (error) {
                defered.reject(error);
            };

            navigator.geolocation.getCurrentPosition(onSuccess, onError);
            return defered.promise;
        }
    }

    return api;
})

angular.module('starter.services')

.factory('Price', function ($http, JsonApiDataStore, $q, Location) {
    var apiUrl = 'http://prodlook.dev.ajieks.ru/api/v1/';

    var attrs = ['cost', 'lat', 'lon', 'dg-id', 'name']

    var Price = function (priceable, values) {
        values = typeof values !== 'undefined' ? values : {};
        if (priceable) {
            var newModel = new JsonApiDataStore.Model('prices')
            attrs.forEach(function (attr) {
                if (values[attr]) {
                    newModel.setAttribute(attr, values[attr]);
                } else {
                    newModel.setAttribute(attr, '');
                }
            })
            newModel.setRelationship('priceable', priceable);
            this.setData(newModel);
        }
    };

    Price.prototype.setData = function (data) {
        angular.extend(this, data);
        if (data.serialize) {
            this.serialize = data.serialize
        }
    };

    Price.prototype.apiUrl = function () {
        return apiUrl + this.priceable._type + '/' + this.priceable.id + '/prices'
    };

    Price.prototype.create = function () {
        var _ = this;
        var defered = $q.defer();
        var onServer = function () {
            var data = _.serialize();
            $http.post(_.apiUrl(), JSON.stringify(data)).success(function (r) {
                defered.resolve(r);
            }).error(function (data, status, headers, config) {
                defered.reject(data);
            });
        }
        var onSuccess = function (position) {
            _.lat = position.latitude;
            _.lon = position.longitude;
            onServer();
        };

        Location.get().then(onSuccess, onServer)
 
        return defered.promise;
    };

    return Price;
})
