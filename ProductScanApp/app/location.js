﻿angular.module('starter.services')

.factory('Location', function ($q) {

    var api = {
        get: function () {
            var defered = $q.defer();
            var onSuccess = function (position) {      
                defered.resolve(position.coords);
            };

            var onError = function (error) {
                defered.reject(error);
            };

            navigator.geolocation.getCurrentPosition(onSuccess, onError);
            return defered.promise;
        }
    }

    return api;
})
