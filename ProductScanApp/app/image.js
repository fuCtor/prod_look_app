﻿angular.module('starter.services')

.factory('Image', function ($http, JsonApiDataStore, $q, $log) {
    var apiUrl = 'http://prodlook.dev.ajieks.ru/api/v1/';

    var attrs = ['kind']

    var Image = function (imageable, values) {
        values = typeof values !== 'undefined' ? values : {};

        var newModel = new JsonApiDataStore.Model('images')
        attrs.forEach(function (attr) {
            if (values[attr]) {
                newModel.setAttribute(attr, values[attr]);
            } else {
                newModel.setAttribute(attr, '');
            }
        })

        if (imageable) {
            newModel.setRelationship('imageable', imageable);
        }
        this.setData(newModel);
    };

    Image.prototype.setData = function (data) {
        angular.extend(this, data);
        if (data.serialize) {
            this.serialize = data.serialize
        }
    };

    Image.prototype.apiUrl = function () {
        if (this.imageable && this.imageable.id) {
            return apiUrl + this.imageable._type + '/' + this.imageable.id + '/images/'
        } else {
            return apiUrl + "images/";
        }
    };

    Image.prototype.captureImage = function () {
        var defered = $q.defer();
        console.debug("Capture image start");
        navigator.camera.getPicture(function (imageData) {
            console.debug("Captured image: " + imageData);
            defered.resolve(imageData);
        }, function (message) {
            defered.reject(message);
        }, { quality: 50, destinationType: Camera.DestinationType.FILE_URI });
        return defered.promise;
    };

    Image.prototype.create = function (imageURI) {
        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = imageURI.substr(imageURI.lastIndexOf('/') + 1);
        options.mimeType = "image/jpeg";

        options.params = { kind: this.kind };
        options.chunkedMode = false;

        var defered = $q.defer()

        var _ = this;

        var ft = new FileTransfer();
        ft.upload(imageURI, this.apiUrl(), function (r) {
            data = JSON.parse(r.response);
            _.id = data.id;
            _.sync().then(function () {
                defered.resolve()
            }, function () {
                defered.reject()
            });
        }, function (e) {
            console.error(e);
            defered.reject();
        }, options);

        return defered.promise;
    };

    Image.prototype.sync = function () {
        var _ = this;
        return $http.get(this.apiUrl() + this.id).success(function (data) {
            var image = JsonApiDataStore.store.sync(data);
            _.setData(image);
        }).error(function (data, status, headers, config) {
            // Что-нибудь делаем в случае ошибки
        });
    };

    return Image;

})