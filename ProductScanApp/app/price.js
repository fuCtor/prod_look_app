﻿angular.module('starter.services')

.factory('Price', function ($http, JsonApiDataStore, $q, Location) {
    var apiUrl = 'http://prodlook.dev.ajieks.ru/api/v1/';

    var attrs = ['cost', 'lat', 'lon', 'dg-id', 'name']

    var Price = function (priceable, values) {
        values = typeof values !== 'undefined' ? values : {};
        if (priceable) {
            var newModel = new JsonApiDataStore.Model('prices')
            attrs.forEach(function (attr) {
                if (values[attr]) {
                    newModel.setAttribute(attr, values[attr]);
                } else {
                    newModel.setAttribute(attr, '');
                }
            })
            newModel.setRelationship('priceable', priceable);
            this.setData(newModel);
        }
    };

    Price.prototype.setData = function (data) {
        angular.extend(this, data);
        if (data.serialize) {
            this.serialize = data.serialize
        }
    };

    Price.prototype.apiUrl = function () {
        return apiUrl + this.priceable._type + '/' + this.priceable.id + '/prices'
    };

    Price.prototype.create = function () {
        var _ = this;
        var defered = $q.defer();
        var onServer = function () {
            var data = _.serialize();
            $http.post(_.apiUrl(), JSON.stringify(data)).success(function (r) {
                defered.resolve(r);
            }).error(function (data, status, headers, config) {
                defered.reject(data);
            });
        }
        var onSuccess = function (position) {
            _.lat = position.latitude;
            _.lon = position.longitude;
            onServer();
        };

        Location.get().then(onSuccess, onServer)
 
        return defered.promise;
    };

    return Price;
})
