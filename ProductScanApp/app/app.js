

angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'beauby.jsonApiDataStore'])

.run(function ($ionicPlatform, $http) {
    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && cordova.platformId === 'ios' && window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }

        var e = document.createElement("script");
        e.setAttribute("src", "http://192.168.1.254:8081/target/target-script-min.js#anonymous");
        document.getElementsByTagName("body")[0].appendChild(e);
        
    });
    
})

.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $stateProvider

    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'AppCtrl'
    })

    .state('app.search', {
        url: '/search',
        views: {
            'menuContent': {
                templateUrl: 'templates/search.html'
            }
        }
    })

    .state('app.scan', {
        url: '/scan',
        views: {
            'menuContent': {
                templateUrl: 'templates/scan.html',
                controller: 'ScanCtrl'
            }
        }
    })
      .state('app.products', {
          url: '/products',
          views: {
              'menuContent': {
                  templateUrl: 'templates/products.html',
                  controller: 'ProductsCtrl'
              }
          }
      })

    .state('app.product', {
        url: '/products/:productId',
        views: {
            'menuContent': {
                templateUrl: 'templates/product.html',
                controller: 'ProductCtrl'
            }
        }
    })

    .state('app.images', {
        url: '/products/:productId/images',
        views: {
            'menuContent': {
                templateUrl: 'templates/images.html',
                controller: 'ProductCtrl'
            }
        }
    })
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/app/products');

    $httpProvider.interceptors.push(function(){
        return {
            request: function(config) {                
                if(config.url.match(/\/api\/v1\//))
                {
                    config.headers['Content-Type'] = 'application/vnd.api+json'
                    config.headers['Accept'] = 'application/vnd.api+json'
                    if (window.device)
                    {
                        config.headers['X-Device-Id'] = device.uuid;
                        config.headers['X-Device-Platform'] = device.platform;
                    }
                }                    
                
                return config;
            }
        }
    })
 
})
