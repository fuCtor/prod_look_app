﻿angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

})

.controller('ProductsCtrl', function($scope, FoodStaff) {
    $scope.products = [];
        
    $scope.$watch('query', function (value) {
        if (value && value.length > 0) {
            FoodStaff.filter(value).then(function (items) {
                $scope.products = items;
            })
        } else {
            $scope.products = []
        }
    })
})

.controller('ProductCtrl', function ($scope, $stateParams, $log, $ionicPopup, $ionicScrollDelegate, DG, FoodStaff) {
    FoodStaff.get($stateParams.productId).then(function (product) {        
        $scope.product = product;
        
    }, function() {
        if($stateParams.productId.length > 12)
        {
            $scope.product = FoodStaff.new({ barcode: $stateParams.productId })
            $scope.new_cost = null
            $scope.current_spinner = ''
            $scope.images = {}
        }
    })

    //4601075347524

    $scope.doRefresh = function () {
        if ($scope.product.id)
        {
            $scope.product.sync().finally(function () {
                $scope.$broadcast('scroll.refreshComplete');
            });
        } else {
            $scope.$broadcast('scroll.refreshComplete');
        }                 
    };

    $scope.complete = function () {
        var cost = $scope.product.new_cost;
        $scope.product.create().then(function (product) {
            $scope.product = product;
            $scope.product.setCost(cost)

        })
    }

    $scope.addImage = function (kind) {
        $scope.current_spinner = kind
        setTimeout(function () {
            console.debug("Start Add Image");
            $scope.product.addImage(kind || 'other').then(function (image) {
                $scope.images[kind] = image.image;                
                $scope.current_spinner = ''
            });
        }, 1);
    }

    $scope.updatePrice = function () {
        $scope.data = { current_adr: {} };

        DG.search().then(function (data) {
            $scope.data.adrs = data;
        })

        // An elaborate, custom popup
        var pricePopup = $ionicPopup.show({
            //template: '<input type="number" ng-model="data.cost">',
            templateUrl: 'templates/price_popup.html',
            title: 'Введите цену товара',
            subTitle: 'только целую часть',
            scope: $scope,
            buttons: [
              { text: 'Отмена' },
              {
                  text: '<b>Готово</b>',
                  type: 'button-positive',
                  onTap: function (e) {
                      if (!$scope.data.cost) {
                          e.preventDefault();
                      } else {
                          return $scope.data.cost;
                      }
                  }
              }
            ]
        });

        pricePopup.then(function (cost) {
            if (cost)
            {
                $scope.product.setCost(cost, { 'dg-id': $scope.data.current_adr.id, name: $scope.data.current_adr.name })
            }
        });
    }

    window.current_scope = $scope;
    window.dg = DG;
})

.controller('ScanCtrl', function ($scope, $location, $stateParams, $log) {
    $scope.scan = function () {
        console.debug("Start SCAN")

        function success(result) {            
            $scope.result = result
            if (result.cancelled) {
                return;
            }
            if (result.format == 'EAN_13') {
                $location.path("/app/products/" + result.text);
            } else {
                $scope.scan();
            }
        }

        if (cordova && cordova.plugins.barcodeScanner) {
            cordova.plugins.barcodeScanner.scan(success, function (error) {
                console.debug("Scanning failed: " + error);
            });
        } else {
            success({ cancelled: false, text: '4601075347524', format: 'EAN_13' });
        }
    }
})