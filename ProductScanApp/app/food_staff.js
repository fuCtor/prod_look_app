angular.module('starter.services')

.factory('FoodStaff', function ($http, JsonApiDataStore, $q, $log, Price, Image) {

    var apiUrl = 'http://prodlook.dev.ajieks.ru/api/v1/food-staffs/';

    var FoodStaff = function (data) {
        if (data) {
            this.setData(data);
        }
    };

    var includes = '?include=promo-image,current-price,images,last-prices'

    FoodStaff.prototype.setData = function (data) {
        angular.extend(this, data);
        if (data.serialize) {
            this.serialize = data.serialize
        }
        if (data.setRelationship) {
            this.setRelationship = data.setRelationship
        }
    };

    FoodStaff.prototype.setCost = function (cost, values) {
        values = typeof values !== 'undefined' ? values : {};
        values['cost'] = cost
        var newCost = new Price(this, values)
        var staff = this;
        return newCost.create().then(function () {
            staff.sync();
        });
    };

    FoodStaff.prototype.minPrice = function () {
        return this['last-prices'][0];
    };

    FoodStaff.prototype.maxPrice = function (cost, values) {
        return this['last-prices'][this['last-prices'].length - 1];
    };

    FoodStaff.prototype.delete = function () {
        $http.delete(apiUrl + this.id).success(function () {
            // ���-������ ������������ �������� ������
        }).error(function (data, status, headers, config) {
            // ���-������ ������ � ������ ������
        });
    };

    FoodStaff.prototype.sync = function () {
        var _ = this;
        return $http.get(apiUrl + this.id + includes).success(function (data) {
            var staff = JsonApiDataStore.store.sync(data);
            _.setData(staff);
        }).error(function (data, status, headers, config) {
            // ���-������ ������ � ������ ������
        });
    };

    FoodStaff.prototype.update = function () {
        return $http.put(apiUrl + this.id, this).success(function () {
            // ���-������ ������������ �������� ������
        }).error(function (data, status, headers, config) {
            // ���-������ ������ � ������ ������
        });
    };

    FoodStaff.prototype.create = function () {
        var data = this.serialize();
        var deferred = $q.defer();
        var _ = this;
        $http.post(apiUrl + includes, JSON.stringify(data)).success(function (r) {
            var staff = JsonApiDataStore.store.sync(r);
            _.setData(staff);
            deferred.resolve(_);
        }).error(function (data, status, headers, config) {
            deferred.reject(data);
        });
        return deferred.promise
    };

    FoodStaff.prototype.addExImage = function (newImage) {
        if (this.images) {
            
            this.images.push(newImage)
        } else {
            this.setRelationship('images', [newImage]);
        }
    }

    FoodStaff.prototype.addImage = function (kind) {
        var newImage = new Image(this, { kind: kind || 'other' });
        var defered = $q.defer();

        $log.debug("Add image with kind: " + newImage.kind);

        var _ = this;

        newImage.captureImage().then(function (imageData) {
            newImage.create(imageData).then(function () {
                console.debug("Image created");
                _.addExImage(newImage);
                defered.resolve(newImage);
            }, function () {
                defered.reject();
            })
        }, function () {
            defered.reject();
        })
        return defered.promise;

    };

    FoodStaff.prototype.imageByKind = function (kind) {;
        for(var i = 0; i < this.images.length; i++){
            if(this.images[i].kind == kind)
            {
                return this.images[i];
            }
        }
        return null;
    };

    var attrs = ['barcode']

    var food_staffs = {
        new: function (values) {
            values = typeof values !== 'undefined' ? values : {};

            var newModel = new JsonApiDataStore.Model('food-staffs')
            attrs.forEach(function (attr) {
                if (values[attr]) {
                    newModel.setAttribute(attr, values[attr]);
                } else {
                    newModel.setAttribute(attr, '');
                }
            })
            return new FoodStaff(newModel);
        },
        filter: function(filter){
            var deferred = $q.defer();
            var scope = this;
            var staffs = [];

            $http.get(apiUrl + "?filter[query]=" + encodeURIComponent(filter)).then(function (response) {
                JsonApiDataStore.store.sync(response.data).forEach(function (data) {
                    staffs.push(new FoodStaff(data));
                });
                deferred.resolve(staffs);
            }, function () {
                deferred.reject();
            })

            return deferred.promise;
        },
        all: function () {
            var deferred = $q.defer();
            var scope = this;
            var staffs = [];

            $http.get(apiUrl).then(function (response) {
                JsonApiDataStore.store.sync(response.data).forEach(function (data) {
                    staffs.push(new FoodStaff(data));
                });
                deferred.resolve(staffs);
            }, function () {
                deferred.reject();
            })

            return deferred.promise;
        },
        get: function (id) {
            var deferred = $q.defer();
            var scope = this;
            var data = {};
            $http.get(apiUrl + id + includes).success(function (data) {
                var staff = JsonApiDataStore.store.sync(data);
                deferred.resolve(new FoodStaff(staff));
            })
            .error(function () {
                deferred.reject();
            });
            return deferred.promise;
        }
    };

    return food_staffs;
})
