﻿angular.module('starter.services', [])

.factory('DG', function ($http, $q, Location) {
    var APIKey = 'ruzqvd2307';
    var apiUrl = 'http://catalog.api.2gis.ru/search';

    function buildUrl(params) {
        var url = apiUrl + "?";
        params['version'] = 1.3
        params['key'] = APIKey
        params['sort'] = 'distance'

        var query = '';

        for(attr in params)
        {
            if(query.length != 0)
            {
                query += '&';
            }

            query += attr + '=' + encodeURIComponent(params[attr])
        }

        return url + query;
    }

    var api = {
        search: function () {

            var defered = $q.defer();
            Location.get().then(function (pos) {
                var url = buildUrl({
                    point: pos.longitude + ',' + pos.latitude,
                    radius: '250',
                    what: 'продукты'
                });

                $http.get(url).then(function (r) {
                    var data = r.data
                    if (data.response_code = '200')
                    {
                        defered.resolve(data.result);
                    } else {
                        defered.reject();
                    }                    
                }, function () {
                    defered.reject();
                });
            }, function () {
                defered.reject();
            })

            return defered.promise;
        }
    }

    return api;
})
